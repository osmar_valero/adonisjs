'use strict'

const User = use('App/Models/User');

class UserController {
	async login({request, auth}) {
		const {email, password} = request.all();
		const token = await auth.attempt(email, password);
		return token;
	}

	async store({request}) {
		const {email, password} = request.all();
		
		const user = await User.create({
			email,
			password,
			username: email
		});
//https://www.youtube.com/watch?v=_dQY8tcQdRg&list=PLs-v5LWbw7JkPXd3q_F-18S0cGy-Z2o8l&index=8
		return this.login(...arguments);
		
	}
}

module.exports = UserController
