'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.create('projects', (table) => {
      table.uuid('id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.uuid('user_id').references('id').inTable('users')
      table.string('name', 80).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('projects')
  }
}

module.exports = ProjectSchema
